package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "attributeType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = FilterNumericAttributeInstance.class, name = "NUMERIC"),
        @JsonSubTypes.Type(value = FilterBinaryAttributeInstance.class, name = "BINARY"),
        @JsonSubTypes.Type(value = FilterTextualAttributeInstance.class, name = "TEXT"),
        @JsonSubTypes.Type(value = FilterMetaAttributeInstance.class, name = "META"),
})

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class FilterAttributeInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    private String attributeId;
    private boolean active;
    @Enumerated(EnumType.STRING)
    private FilterAttributeType filterAttributeType;

    @Enumerated(EnumType.STRING)
    private FilterAttributeState state;

    public FilterAttributeInstance copy() {
        FilterAttributeInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<FilterAttributeInstance> copy(List<FilterAttributeInstance> instances) {
        return instances.stream().map(instance -> instance.copy()).toList();
    }
}
