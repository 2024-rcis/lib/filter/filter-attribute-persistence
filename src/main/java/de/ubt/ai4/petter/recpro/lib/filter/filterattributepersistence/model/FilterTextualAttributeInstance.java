package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("TEXT")
public class FilterTextualAttributeInstance extends FilterAttributeInstance {
    private String value;
    private boolean valueExact;
    private boolean valueContains;
    private boolean caseSensitive;
}
