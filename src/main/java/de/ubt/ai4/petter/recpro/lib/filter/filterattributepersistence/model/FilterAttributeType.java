package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

public enum FilterAttributeType {
    TEXT,
    NUMERIC,
    OBJECT,
    BINARY,
    META
}
