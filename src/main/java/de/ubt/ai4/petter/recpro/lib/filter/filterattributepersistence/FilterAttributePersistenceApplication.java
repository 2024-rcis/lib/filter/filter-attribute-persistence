package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilterAttributePersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilterAttributePersistenceApplication.class, args);
	}

}
